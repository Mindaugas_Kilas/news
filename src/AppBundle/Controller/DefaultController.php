<?php

namespace AppBundle\Controller;

use AppBundle\Form\SubscriberType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $form = $this->createForm(SubscriberType::class);
        return $this->render(
            'AppBundle:frontend/subscribers:index.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    public function saveAction(Request $request)
    {

        $form = $this->createForm(SubscriberType::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $this->get('app.service.file')->save($data);

        } else {
            return $this->render(
                'AppBundle:frontend/subscribers:index.html.twig',
                [
                    'form' => $form->createView(),
                ]
            );
        }
        return $this->redirectToRoute('homepage');
    }
}

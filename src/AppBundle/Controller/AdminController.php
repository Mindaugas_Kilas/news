<?php

namespace AppBundle\Controller;

use AppBundle\Form\SubscriberType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    public function indexAction()
    {
        $subscribers = $this->get('app.service.file')->listData();
        $categories = $this->getParameter('news_categories');
        return $this->render(
            'AppBundle:frontend/admin:index.html.twig',
            [
                'subscribers' => (array) $subscribers,
                'categories'=> $categories,
            ]
        );
    }

    public function updateAction(Request $request, $id)
    {
        $form = $this->createForm(SubscriberType::class);
        $data = (array) $form->handleRequest($request)->getViewData();
        if ($form->isValid()) {

            $this->get('app.service.file')->save($data, $id);
            return $this->redirectToRoute('subscribers');
        }
        return $this->render(
            'AppBundle:frontend/admin:edit.html.twig',
            [
                'form' => $form->createView(),
                'id' => $id
            ]
        );
    }

    public function editAction($id)
    {
        $subscriber = $this->get('app.service.file')->getSubscriber($id);
        $form = $this->createForm(SubscriberType::class, $subscriber);
        return $this->render(
            'AppBundle:frontend/admin:edit.html.twig',
            [
                'form' => $form->createView(),
                'id' => $id
            ]
        );
    }

    public function deleteAction($id)
    {
        $this->get('app.service.file')->deleteSubscriber($id);
        return $this->redirectToRoute('subscribers');
    }
}


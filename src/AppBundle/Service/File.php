<?php

namespace AppBundle\Service;


use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class File implements ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private $log_file, $fp;

    public function save($data, $id = false)
    {
        $this->lfile('src\AppBundle\Files\subscribers.txt');
        $new_data = $this->formatData($data, $id);
        $this->lwrite($new_data);
    }

    public function lfile($path)
    {
        $this->log_file = $this->container->getParameter('root_path').$path;
    }
    public function lwrite($data) {
        if (!is_resource($this->fp)) {
            $this->lopen();
        }
        fwrite($this->fp, json_encode($data));
    }
    public function lclose()
    {
        fclose($this->fp);
    }
    private function lopen()
    {
        $this->fp = fopen($this->log_file, 'c+') or exit("Can't open $this->log_file!");
    }

    private function lclear()
    {
        $this->fp = fopen($this->log_file, 'w+');
    }

    public function lread()
    {
        if (!is_resource($this->fp)) {
            $this->lopen();
        }
        return fread($this->fp,1024);
    }
    public function formatData($data, $id = false)
    {
        if(!$id){
            $id = "nr" . time();
        }
        $old_data = json_decode($this->lread());
        $this->lclear();
        return array_merge((array) $old_data, [$id=>$data]);
    }

    public function listData()
    {
        $this->lfile('src\AppBundle\Files\subscribers.txt');
        $data = json_decode($this->lread());
        return $data;
    }

    public function getSubscriber($id)
    {
        $this->lfile('src\AppBundle\Files\subscribers.txt');
        $subscribers = json_decode($this->lread());
        return $subscribers->$id;
    }

    public function deleteSubscriber($id)
    {
        $this->lfile('src\AppBundle\Files\subscribers.txt');
        $data = json_decode($this->lread());
        unset($data->$id);
        $this->lclear();
        $this->lwrite($data);
    }

}
<?php

namespace AppBundle\Form;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;

class SubscriberType extends AbstractType implements ContainerAwareInterface
{
    /**
     * @var ContainerAwareInterface
     */
    private $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $categories = [];
        $news_categories = $this->container->getParameter('news_categories');

        foreach ($news_categories as $key => $category){
            $categories[$key] = $category;
        }
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'required' => true,
                    'constraints' => [
                        new Length(['min' => 3]),
                    ]
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'required' => true,
                    'constraints' => [
                        new Email(
                            [
                                'message' => 'The email "{{ value }}" is not a valid email.',
                            ]
                        )
                    ]
                ]
            )
            ->add(
                'categories',
                ChoiceType::class,
                [
                    'multiple' => true,
                    'required' => true,
                    'label_attr' => array('class' => 'snow'),
                    'label' => 'Categories',
                    'choices' => $categories,
                ]
            )
            ->add(
                'submit',
                SubmitType::class,
                [
                    'label' => 'Save',
                    'attr' => array('class' => 'button first-form-button'),
                ]
            )
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'app_bundle_subscriber_type';
    }
}
